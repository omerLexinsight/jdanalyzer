import pandas as pd
from nltk.util import ngrams
from collections import Counter
from functools import reduce
from collections import OrderedDict
import nltk
import re
from ExcelOperations import *

# pip install pandas
# pip install nltk

def loadData(link):
    return pd.read_csv(link)


def ReduceData(data):
    # naming columns
    data.columns = ['Jobs', 'Descriptions']
    # Step1: Extracting Job Descriptions
    data['Descriptions']
    keywords = ["attorney", "lawyer", "atty", "paralegal", "barrister", "jurist", "solicitor", "legal advisor",
                "counselor", "advocate", "mediator", "arbitrator", "attorney-at-law", "adjudicator"]
    # Changing Jobs and Keywords uppercase
    keywords = [x.upper() for x in keywords]
    data['Jobs'] = data['Jobs'].apply(lambda s: s.upper() if type(s) == str else s)
    # Step 2: Identify the ‘Relevant’ Job Descriptions
    data = data[reduce(lambda a, b: a | b, (data['Jobs'].str.contains(s) for s in keywords))]
    return data


def getFirstTwoSentences(data):
    # Step 3: Extract the first two sentences, including the headings, from all ‘Relevant’ Job Descriptions
    data['Descriptions'] = data['Descriptions'].apply(lambda x: ' '.join(re.split(r'(?<=[.])\s', x)[:2]))
    return data


def cleanData(data):
    # Step 4: process the text suitably before printing the N-Grams
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', data)
    cleantext = re.sub(r"\b[a-zA-Z]\b", "", cleantext)
    tokens = nltk.wordpunct_tokenize(cleantext)
    text = nltk.Text(tokens)
    words = [w.lower() for w in text if w.isalpha()]
    return words


def getBigrams(words):
    # Step 5: Print a list of  bigrams
    df = pd.DataFrame()
    bigrams = dict((Counter(ngrams(words, 2))))
    d_sorted_by_value = dict(OrderedDict(sorted(bigrams.items(), key=lambda x: x[1], reverse=True)))
    # for keys, value in d_sorted_by_value.items():
    #    print(keys[0] + ' ' + keys[1], value)
    df["Bigrams"] = d_sorted_by_value.keys()
    df["Frequency"] = d_sorted_by_value.values()
    return df


def getUnigrams(words):
    # Step 5:  Print a list of unigrams
    df = pd.DataFrame()
    unigrams = dict((Counter(ngrams(words, 1))))
    d_sorted_by_value = dict(OrderedDict(sorted(unigrams.items(), key=lambda x: x[1], reverse=True)))
    # for keys, value in d_sorted_by_value.items():
    #    print(keys[0], value)
    df["Unigrams"] = d_sorted_by_value.keys()
    df["Frequency"] = d_sorted_by_value.values()
    return df


def getTrigrams(words):
    # Step 5:  Print a list of trigrams
    df = pd.DataFrame()
    trigrams = dict((Counter(ngrams(words, 3))))
    d_sorted_by_value = dict(OrderedDict(sorted(trigrams.items(), key=lambda x: x[1], reverse=True)))
    # for keys, value in d_sorted_by_value.items():
    #    print(keys[0], value)
    df["Trigrams"] = d_sorted_by_value.keys()
    df["Frequency"] = d_sorted_by_value.values()
    return df


path = "/Users/omerorhan/jdanalyzer/gloves/ngrams/";


# dataset = loadData(path + "smallcsv.csv")
# dtTitle = dataset["Title"]
# print(dtTitle)
##dtTitle.to_csv(path + "titles.csv")
# words = cleanData(dtTitle.to_string())
# getUnigrams(words).sort_values("Frequency", ascending=False).to_csv(path + "unigrams.csv", index=False)
# getBigrams(words).sort_values("Frequency", ascending=False).to_csv(path + "bigrams.csv", index=False)
# getTrigrams(words).sort_values("Frequency", ascending=False).to_csv(path + "trigrams.csv", index=False)

def extractngramssummary():
    import glob
    files = glob.glob(path + "*grams.csv")  # picking up XML files in folder
    excelfile = path + "summary_ngrams.xlsx"
    sheets = getSheetNames(excelfile)
    for sheet in sheets:
        data = pd.read_csv(path + sheet + ".csv", nrows=10000)
        writeExcelData(data, excelfile, sheet, 1, 1)


path = "/mnt/usb-ssd/csvfiles/"
excelfile = path + "functions&industries_list.xlsx"

import numpy as np

