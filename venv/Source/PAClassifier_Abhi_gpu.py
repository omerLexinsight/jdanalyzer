#!/usr/bin/env python
# coding: utf-8

# ### Create Deep Learning/Keras Model for Practice Area Classification from Job Descriptions
# #### This version also provides word relevance based explanations for the classification
# Run notebook named PracticePrepLearning to gather all data this notebook needs
# Note that the glove.6B data isn't checked in to github. You need to download and unzip it separately.
# This code is by Francois Chollet, the author of Keras himself, and is discussed in his blog at
# https://blog.keras.io/using-pre-trained-word-embeddings-in-a-keras-model.html
# and heavily extended by MLA (Monica Anderson)
# 
# This notebook creates a "pamm-*" timestamped directory containing the resulting model and support files.
# These can be copied to the runtime data directory in clsrv/clsrv/data for production/runtime use
# 
# This notebook handles single or multiple labels per document (i.e. single-label or multi-label) 
# and both the binary (2 known labels) and multi-class (more than 2 known labels) cases.

# In[9]:


# Widen our notebook to full width. Package is also used for html display of highlighted text
# from IPython.core.display import display, HTML
# display(HTML("<style>.container { width:100% !important; }</style>"))


# In[10]:

import os

os.environ["CUDA_VISIBLE_DEVICES"] = '0'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import os
import os.path
import sys

print(sys.version)
import subprocess

os.environ['KERAS_BACKEND'] = 'tensorflow'
import os, sys, json, pickle, time, datetime, matplotlib, shutil
import keras
import numpy as np
import pandas as pd
import tensorflow as tf
import regex as re
import html
# import IPython
import faulthandler
# from deepexplain.tensorflow import DeepExplain
from matplotlib import pyplot as plt
from collections import Counter
from sklearn.preprocessing import normalize, MultiLabelBinarizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical, plot_model
from keras.layers import Dense, Input, GlobalMaxPooling1D, Conv1D, MaxPooling1D, Embedding, Activation
from keras.models import Model, save_model
from keras.initializers import Constant
from keras.callbacks import Callback, EarlyStopping
from keras import backend as K
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
keras.backend.set_session(sess)

# In[11]:


'''This script loads pre-trained word embeddings (GloVe embeddings) into a frozen Keras Embedding layer

GloVe embedding data can be found at:
http://nlp.stanford.edu/data/glove.6B.zip
(source page: http://nlp.stanford.edu/projects/glove/)
'''
os.chdir("/mnt/usb-ssd/Abhi/Resume/")
subprocess.call("pwd", shell=True)

# faulthandler.enable()
starttime = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')
experiment_id = "pass"  # TUNEEXPERIMENTID Practice area multi-tag multi-class. Used to provide unique names to files and directories
resultsdir = experiment_id + "-" + starttime + "/"
# os.mkdir(resultsdir)
# os.system("bin/logexp")            # Gather hyperparameters with tune comments into resultsdir
# numberrows = None                  # Limit input during development to some number of rows. Or None for whole file

BASE_DIR = ''
GLOVE_DIR = os.path.join(BASE_DIR, 'Data/')

multi_label = True  # TUNEMULTILABEL Automatically set to true if we have multiple labels for each document (both input and output)
if multi_label:
    csvlearnpath = "/mnt/m2-ssd/omer/jd-classifier/trainingjD_1.csv"  # TUNECORPUSPATH if multi_label
else:
    csvlearnpath = "paslearn.csv"  # TUNECORPUSPATH if not multi_label

MAX_SEQUENCE_LENGTH = 1000  # TUNESEQLEN
MAX_NUM_WORDS = 4000  # TUNENUMWORDS
EMBEDDING_DIM = 100  # TUNEEMBEDDINGDIM
REQUESTEDEPOCHS = 30  # TUNEEPOCHS
VALIDATION_SPLIT = 0.2

# first, build index mapping words in the embeddings set to their embedding vector
glovectorpath = os.path.join(GLOVE_DIR, 'glove.6B.100d.txt')  # Original in Chollet's code
# glovectorpath = "livectors.txt"                                              # TUNEGLOVE Our own GloVe file - "lexinsight vectors"

print('Indexing word vectors in ', glovectorpath)

embeddings_index = {}
with open(glovectorpath, encoding="utf8") as f:
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs

print('Found %s word vectors.' % len(embeddings_index))

# second, prepare text samples and their labels
print('Loading Kaggle Job Posting Data corpus from ' + csvlearnpath)
corpus = pd.read_csv(csvlearnpath, dtype={0: 'str', 1: 'str'})
corpus = corpus.T.squeeze()
corpus = corpus.str.split(pat=";", n=1, expand=True)
corpus = corpus.rename(columns={0: "label", 1: "text"})
print(corpus.head(5))

corpustags = []  # Here we use the term "tag" for numeric labels
corpuslabels = []  # We use the term "label" for string labels, even though Chollet uses 'labels' for numeric labels
knownlabels = []  # Index into this to get string label for any numeric tag
labelset = set()  # Used to ensure unique labels while reading file
labelcounts = Counter()  # Occurrence counter for all knowns labels
corpuslabelsastext = corpus['label']
texts = corpus['text']  # list of text samples
# debugfile2 = open(resultsdir + "debug2", "w")

for rownumber in range(len(corpuslabelsastext)):
    labeltext = corpuslabelsastext[rownumber]
    if labeltext == None:
        print("No labels provided for corpus row ", rownumber)
        sys.exit(-1)  # This is fatal because we depend on texts[] being the complete corpus with label(s) for each row

    text = texts[rownumber]
    labeltext = str(labeltext).strip()

    labeltextlist = labeltext.split('|')
    if len(labeltextlist) > 1 and not multi_label:
        textsample = text
        if len(textsample) > 100:
            textsample = text[0:99]
        print("Discovered multi-label input (separated by | ) when supposedly processing " +
              "in single-label mode - line %d, text starts as %s: " % (rownumber, textsample))
        sys.exit(-1)
    labellist = []
    for label in labeltextlist:
        label = str(label).strip()
        labellist.append(label)
        labelcounts[label] += 1
        if not label in labelset:
            labelset.add(label)  # Note that we have found label
            knownlabels.append(label)  # Append to list of unique labels we have encountered, in given order
    corpuslabels.append(labellist)

print("Done")

print("\n=============================================\n            All Known PA Labels")
print("Tag   Count    Label\n=============================================")
index = 0
for label in knownlabels:
    print("%3d  %6d    %s" % (index, labelcounts[label], label))
    index += 1
print("=============================================\n\n")

print(len(corpuslabelsastext))


def to_category_vector(categories, target_categories):
    vector = np.zeros(len(target_categories)).astype(np.float32)
    for i in range(len(target_categories)):
        if target_categories[i] in categories:
            vector[i] = 1.0
    # vector = [float(i)/sum(vector) for i in vector]
    return vector


labels = []
max_length = max(len(labellist) for labellist in corpuslabels)
print("Most labels per document: ", max_length)
for labellist in corpuslabels:  # This list will have length > 1 only for multi-label input
    doclabels = []
    doctags = []
    for label in labellist:
        doclabels.append(label)
    labels.append(to_category_vector(doclabels, knownlabels))
labels = np.array(labels)

print("Labels are", labels[0])


class Metrics(Callback):
    def on_train_begin(self, logs={}):
        self.epochs = []
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []

    def on_epoch_end(self, epoch, logs={}):
        val_predict = np.argmax(self.model.predict(self.validation_data[0]), axis=1)
        val_targ = np.argmax(self.validation_data[1], axis=1)
        _val_f1 = f1_score(val_targ, val_predict, average='macro')
        _val_recall = recall_score(val_targ, val_predict, average='macro')
        _val_precision = precision_score(val_targ, val_predict, average='macro')
        self.epochs.append(epoch)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print(" val_f1: {0:.4f} - val_precision: {1:.4f} — val_recall {2:.4f}".format(_val_f1, _val_precision,
                                                                                      _val_recall))
        if len(self.epochs) < REQUESTEDEPOCHS:  # Don't do this last epoch to avoid duplication
            # Print graph for F1, Precision and Recall
            plt.clf()
            plt.plot(self.epochs, self.val_f1s, "k", label="F1")
            plt.plot(self.epochs, self.val_precisions, "b", label="Precision")
            plt.plot(self.epochs, self.val_recalls, "g", label="Recall")
            plt.title("F1, Precision, and Recall")
            plt.xlabel("Epoch")
            plt.ylabel("F1, Precision, and Recall")
            plt.legend()
            plt.show()


metrics = Metrics()


class MultiMetrics(Callback):
    def on_train_begin(self, logs={}):
        self.epochs = []
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []

    def on_epoch_end(self, epoch, logs={}):
        val_predict = np.round(self.model.predict(self.validation_data[0]))
        val_targ = self.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict, average='weighted')
        _val_recall = recall_score(val_targ, val_predict, average='weighted')
        _val_precision = precision_score(val_targ, val_predict, average='weighted')
        self.epochs.append(epoch)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print(" val_f1: {0:.4f} - val_precision: {1:.4f} — val_recall {2:.4f}".format(_val_f1, _val_precision,
                                                                                      _val_recall))

        if len(self.epochs) < REQUESTEDEPOCHS:  # Don't do this last epoch to avoid duplication
            # Print graph for F1, Precision and Recall
            plt.clf()
            plt.plot(self.epochs, self.val_f1s, "k", label="F1")
            plt.plot(self.epochs, self.val_precisions, "b", label="Precision")
            plt.plot(self.epochs, self.val_recalls, "g", label="Recall")
            plt.title("F1, Precision, and Recall")
            plt.xlabel("Epoch")
            plt.ylabel("F1, Precision, and Recall")
            plt.legend()
            plt.show()


multimetrics = MultiMetrics()


# In[ ]:


def printfinalgraphs(history):
    # History: {
    #    'val_loss': [4.21230534171123, 3.257249369201184],
    #    'val_binary_accuracy': [0.9199270336930101, 0.9411962336407181],
    #     'val_categorical_accuracy': [0.09924231167224626, 0.11543604212863859],
    #     'loss': [4.905415511810408, 3.710341749558913], 
    #     'binary_accuracy': [0.7818576617144749, 0.9352922894728278], 
    #     'categorical_accuracy': [0.10495042154819252, 0.10197942582465318]}

    if multi_label:
        epochs = multimetrics.epochs
    else:
        epochs = metrics.epochs
    # Plot accuracy
    plt.plot(epochs, history['categorical_accuracy'], 'b', label='Categorical acc')
    plt.plot(epochs, history['val_categorical_accuracy'], 'k', label='Validated cat. acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.show()

    # Print graphs for Loss
    plt.clf()
    plt.plot(epochs, history['loss'], "b", label="Training loss")
    plt.plot(epochs, history['val_loss'], "k", label="Validation loss")
    plt.title("Training and Validation loss")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()

    # We are getting too many of this one. 
    # Print graphs for F1, Precision and Recall
    # plt.clf()
    # plt.plot(epochs, multimetrics.val_f1s, "k", label = "F1")
    # plt.plot(epochs, multimetrics.val_precisions, "b", label = "Precision")
    # plt.plot(epochs, multimetrics.val_recalls, "g", label = "Recall")
    # plt.title("F1, Precision, and Recall")
    # plt.xlabel("Epoch")
    # plt.ylabel("F1, Precision, and Recall")
    # plt.legend()
    # plt.show()

    # Do it again for the file output. First accuracy
    plt.clf()
    plt.plot(epochs, history['categorical_accuracy'], 'b', label='Categorical acc')
    plt.plot(epochs, history['val_categorical_accuracy'], 'k', label='Validated cat. acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.savefig(resultsdir + "Accuracy.png")

    # Print graphs for Loss
    plt.clf()
    plt.plot(epochs, history['loss'], "b", label="Training loss")
    plt.plot(epochs, history['val_loss'], "k", label="Validation loss")
    plt.title("Training and Validation loss")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend()
    plt.savefig(resultsdir + "Loss.png")

    if multi_label:
        # Print graphs for F1, Precision and Recall
        plt.clf()
        plt.plot(epochs, multimetrics.val_f1s, "k", label="F1")
        plt.plot(epochs, multimetrics.val_precisions, "b", label="Precision")
        plt.plot(epochs, multimetrics.val_recalls, "g", label="Recall")
        plt.title("F1, Precision, and Recall")
        plt.xlabel("Epoch")
        plt.ylabel("F1, Precision, and Recall")
        plt.legend()
        plt.savefig(resultsdir + "F1PrecisionRecall.png")
    else:
        # Print graphs for F1, Precision and Recall
        plt.clf()
        plt.plot(epochs, metrics.val_f1s, "k", label="F1")
        plt.plot(epochs, metrics.val_precisions, "b", label="Precision")
        plt.plot(epochs, metrics.val_recalls, "g", label="Recall")
        plt.title("F1, Precision, and Recall")
        plt.xlabel("Epoch")
        plt.ylabel("F1, Precision, and Recall")
        plt.legend()
        plt.savefig(resultsdir + "F1PrecisionRecall.png")


print('Found %s texts.' % len(texts))

# vectorize the text samples into a 2D integer tensor
tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
tokenizer.fit_on_texts(texts)
sequences = tokenizer.texts_to_sequences(texts)
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))
knownwords = {}
for w in word_index.keys():
    knownwords[word_index[w]] = w

# print(knownwords)

data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

print('Shape of data tensor:', data.shape)
print('Shape of label tensor:', labels.shape)

# split the data into a training set and a validation set
indices = np.arange(data.shape[0])
np.random.shuffle(indices)
data = data[indices]
labels = labels[indices]
num_validation_samples = int(VALIDATION_SPLIT * data.shape[0])

x_train = data[:-num_validation_samples]
y_train = labels[:-num_validation_samples]
x_val = data[-num_validation_samples:]
y_val = labels[-num_validation_samples:]

print("y_train=", y_train)
print('Preparing embedding matrix.')

# prepare embedding matrix
num_words = min(MAX_NUM_WORDS, len(word_index) + 1)
embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))
for word, i in word_index.items():
    if i >= MAX_NUM_WORDS:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

# load pre-trained word embeddings into an Embedding layer
# note that we set trainable = False so as to keep the embeddings fixed
embedding_layer = Embedding(num_words,
                            EMBEDDING_DIM,
                            embeddings_initializer=Constant(embedding_matrix),
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False,
                            name='embedding')
print('Creating Network.')

# train a 1D convnet with global maxpooling
sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32', name='input_x')
embedded_sequences = embedding_layer(sequence_input)

filters = 400  # TUNEFILTERS
kernelsize = 5  # TUNEKERNEL

x = Conv1D(filters, kernelsize, activation='relu')(embedded_sequences)  # TUNETOPOLOGY
x = MaxPooling1D(kernelsize)(x)  # TUNETOPOLOGY
x = Conv1D(filters, kernelsize, activation='relu')(x)  # TUNETOPOLOGY
x = MaxPooling1D(kernelsize)(x)  # TUNETOPOLOGY
x = Conv1D(filters, kernelsize, activation='relu')(x)  # TUNETOPOLOGY
x = GlobalMaxPooling1D()(x)  # TUNETOPOLOGY
x = Dense(filters, activation='relu')(x)  # TUNETOPOLOGY

# In[ ]:


if multi_label:  # Evaluate each class independently to allow multi-label input and output per document
    print('Learning model for multi label prediction using binary_crossentropy ' +
          'using categorical_accuracy, sigmoid activation, and rmsprop optimizer')
    print(knownlabels)
    l_dense2 = Dense(len(knownlabels), name='dense2')(x)
    l_activ = Activation(activation='sigmoid')(l_dense2)
    model = Model(sequence_input, l_activ)
    # Next lines: See discussion at https://stackoverflow.com/questions/42081257/keras-binary-crossentropy-vs-categorical-crossentropy-performance
    # and at https://stackoverflow.com/questions/41327601/why-is-binary-crossentropy-more-accurate-than-categorical-crossentropy-for-multi/41913968
    # and https://www.depends-on-the-definition.com/guide-to-multi-label-classification-with-neural-networks for the use of binary_crossentropy
    # and https://www.kaggle.com/sushantjha8/multiple-input-and-single-output-in-keras
    # Original: model.compile(loss='binary_crossentropy', optimizer='adam',metrics=['categorical_accuracy'])
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
    # early_stop = EarlyStopping(monitor='val_categorical_accuracy', patience=2, mode='max') 

    hist = model.fit(x_train, y_train,
                     batch_size=512,
                     epochs=REQUESTEDEPOCHS,
                     validation_data=(x_val, y_val),
                     callbacks=[multimetrics])
    from sklearn.metrics import classification_report

    y_val = np.argmax(y_val, axis=1)

    y_pred = model.predict(x_val)
    y_pred = np.argmax(y_pred, axis=1)
    print(classification_report(y_val, y_pred, target_names=knownlabels))

else:  # Single label per document for both input labeling and output classific  knownlabels)
    print("known labels : ", knownlabels)
    if len(knownlabels) > 2:  # multi_class case. This is the case in Chollet's original code

        # We have more than two classes but we want only a single label on each input document
        # and we want a single classification as output.
        print('Learning model for single class prediction among multiple (more than two) classes ' +
              'using categorical_crossentropy, softmax activation, and rmsprop optimizer')
        l_dense2 = Dense(len(knownlabels), name='dense2')(x)
        l_activ = Activation(activation='softmax')(l_dense2)
        model = Model(sequence_input, l_activ)
        # Below, MLA changed metrics from 'acc' because Keras is known to have problems in this case
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['categorical_accuracy'])
        hist = model.fit(x_train, y_train,
                         batch_size=128,
                         epochs=REQUESTEDEPOCHS,
                         validation_data=(x_val, y_val),
                         callbacks=[metrics])
    else:
        # We have only two defined classes with only one label as input per document
        # and we want a single classification as output.
        print('Learning model for single class prediction among two classes ' +
              'using binary_crossentropy, softmax activation, and rmsprop optimizer')
        l_dense2 = Dense(len(knownlabels), name='dense2')(x)
        l_activ = Activation(activation='softmax')(l_dense2)
        model = Model(sequence_input, l_activ)
        model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['binary_accuracy'])
        hist = model.fit(x_train, y_train, batch_size=128,
                         epochs=REQUESTEDEPOCHS,
                         validation_data=(x_val, y_val),
                         callbacks=[metrics])
    from sklearn.metrics import classification_report

    y_pred = model.predict(x_val)
    print(classification_report(y_val, y_pred))

print("Saving model to disk...")
keras.models.save_model(model, resultsdir + "model.h5")  # Save almost everything in one h5 file. What is missing:
with open(resultsdir + 'dictionary.json',
          'w') as dictionary_file:  # Save word_index separately so we can use it in runtime classifier
    json.dump(word_index, dictionary_file)
with open(resultsdir + 'tokenizer.pickle', 'wb') as tokenizer_file:  # Save entire tokenizer separately
    pickle.dump(tokenizer, tokenizer_file, protocol=pickle.HIGHEST_PROTOCOL)
# shutil.copy("code.log", resultsdir + "code.log")                      # Copy our code hyperparameter dump (made by bin/logexp script) to resultsdir
# shutil.copy("KerasPractice.py", resultsdir + "KerasPractice.py")      # Might as well copy all the code we used
print("Saved model and code to disk in directory " + resultsdir)
plot_model(model, to_file=resultsdir + 'model.png')
