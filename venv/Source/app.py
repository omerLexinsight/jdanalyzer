from flask import Flask, render_template
import boto3
from config import S3_BUCKET, S3_KEY, S3_SECRET
from flask_restful import Resource, Api
from flask import Flask


application = Flask(__name__)
api = Api(application)

s3_resource = boto3.resource(
   "s3",
   aws_access_key_id=S3_KEY,
   aws_secret_access_key=S3_SECRET
)


class jdclassifier(Resource):
   def get(self):
      return "ssssa"


api.add_resource(jdclassifier, '/predict', endpoint='bar')

if __name__ == "__main__":
   application.run(debug=True)