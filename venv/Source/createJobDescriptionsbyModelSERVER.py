import pandas as pd

import json
import pickle
import re
import keras
import numpy as np
import pandas as pd
from ExcelOperations import *
import os
global tokenizer, classnames, model, separatorspattern, multispacepattern


#2-Environment setting(Server:S, Local:L)
environment = 'L'
if environment == 'S':
    path = ''
    excelfile ="sales.xlsx"
    os.environ["CUDA_VISIBLE_DEVICES"] = '1'
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    keras.backend.set_session(sess)

elif environment == 'L':
    path = '/Users/omerorhan/jdanalyzer/gloves/salesdata/'
    excelfile =path+ "sales.xlsx"

def isNaN(num):
    return num != num


# reads job fuctions from excel file based on google excel. Before run the function download excel and change the path.
def readjobfunctionsbythebalancecareers():
    diclist = {}
    mustvalues = ""
    datafunctionsprev = ""
    # reads sub functions until find empty rows. If it finds empty rows then looking for job titles.
    for x in range(1, 100):
        datafunctions = readExcelRange(excelfile, "Sales", x, x, 1, 1)
        if str(datafunctions) == "[]" or str(datafunctions[0][0]) == "END":
            break
        if str(datafunctions[0][0]) == "nan":
            # it is for job titles.
            datamust = readExcelRange(excelfile, "Sales", x, x, 2, 2)
            if not str(datamust) == '[]' and not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        else:
            datafunctionsprev = datafunctions
        # understands for new subfunction
        if readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1) != "nan" and str(
                readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1)[0][0]) != "nan":
            if len(mustvalues) > 0 and mustvalues[-1] == "|":
                mustvalues = mustvalues[0:-1]
            diclist.update({str(datafunctionsprev[0][0]): mustvalues})
            mustvalues = ""
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    return diclist

listoftitles = ''
listoftitles = readjobfunctionsbythebalancecareers()
print(listoftitles.keys())

knownlabels = []  # Index into this to get string label for any numeric tag
labels_index = {}  # dict from string label to numeric tag
labelset = set()  # Used to ensure unique labels while reading file

def reallyclean(s):
    breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
    unwantedseparators = re.compile(
        r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
    markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
    multisemipattern = re.compile(r"( *; *)+")
    multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n
    if type(s) is str:
        s =  " " + str(s).lower() + " "
    else:
        s =  " " + str(s) + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s

import keras.backend as K

def precision(y_true, y_pred):
    # Count positive samples.
    c1 = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    c2 = K.sum(K.round(K.clip(y_pred, 0, 1)))
    c3 = K.sum(K.round(K.clip(y_true, 0, 1)))

    # If there are no true samples, fix the F1 score at 0.
    if c3 == 0:
        return 0
    # How many selected items are relevant?
    precision = c1 / c2
    # How many relevant items are selected?
    return precision

def recall(y_true, y_pred):
    # Count positive samples.
    c1 = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    c2 = K.sum(K.round(K.clip(y_pred, 0, 1)))
    c3 = K.sum(K.round(K.clip(y_true, 0, 1)))

    # If there are no true samples, fix the F1 score at 0.
    if c3 == 0:
        return 0
    # How many relevant items are selected?
    recall = c1 / c3
    return recall

def f1_score(y_true, y_pred):
    # Count positive samples.
    c1 = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    c2 = K.sum(K.round(K.clip(y_pred, 0, 1)))
    c3 = K.sum(K.round(K.clip(y_true, 0, 1)))

    # If there are no true samples, fix the F1 score at 0.
    if c3 == 0:
        return 0
    # How many selected items are relevant?
    precision = c1 / c2
    # How many relevant items are selected?
    recall = c1 / c3
    # Calculate f1_score
    f1_score = 2 * (precision * recall) / (precision + recall)
    return f1_score
def readmodel():
    with open(path + 'tokenizer.pickle',
              'rb') as handle:  # Must use a tokenizer with the same dictionary we used for learning
        tokenizer = pickle.load(handle)  # So we pickled what we learned with and then unpickle it here
    with open(path + 'classnames.json', 'r') as classnames_file:
        knownlabels = json.load(classnames_file)  # Load translations from our class indices to practice area names
    print(knownlabels)
    model = keras.models.load_model(path + "model.h5",compile=False)  # Load Keras model we learned using KerasPractice notebook
    print("Loaded model")

for label in knownlabels:
    tag = len(labels_index)
    labels_index[label] = tag  # Construct dict from label (string) to tag (numerical)

#5-practicearea; predict sector names
def practicearea(text):  # Returns practice are given any text.
    global tokenizer, classnames, model
    sequences = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=1000)
    prediction = model.predict(sequences)  # Run the actual classification
    return (list(knownlabels.keys())[list(knownlabels.values()).index(np.argmax(prediction))])

#3-Read and prepare job description from Sample file for prediction
def make_label_tensor(doclabels, allknownlabels):
    tensor = np.zeros(len(allknownlabels)).astype(np.float32)
    for i in range(len(allknownlabels)):
        if allknownlabels[i] in doclabels:
            tensor[i] = 1.0
    return tensor


#texts = open(path + 'sample', 'r') #sampe is just a text file. You can copy your job description in it.
dataset = pd.read_csv(path+"SalesjD.csv",nrows=1000, index_col=False)

print(dataset.shape)
def createjdbymodel():
    df = pd.DataFrame(columns=list(['Label', 'Title', 'Description']))
    print("started prediction")
    count=0
    for index, row in dataset.iterrows():
        count = count+1
        text = reallyclean(row['Description'])
        prediction = practicearea(text)
        for key in listoftitles.keys():
            if prediction == key:
                df = df.append({'Label': key, 'Title': row["Title"], 'Description': row["Description"]},
                               ignore_index=True)
        if count % 1000 == 0:
            print(str(count))
    print(df.shape)
    df.to_csv(path+"jdbymodel.csv", index=False)
#createjdbymodel()




def readjdbymodel():
    dataset = pd.read_csv(path + "jdbymodel.csv", index_col=False)
    datagroup = dataset.groupby("Label").size().reset_index(name="count")
    print(datagroup.sort_values(by='count', ascending=False))
    dataexecutive = dataset[dataset["Label"]=="account executives and advisors"]
    datagrouptitle = dataexecutive.groupby(["Label", "Title"]).size().reset_index(name="count")
    datagrouptitle.sort_values(by='count', ascending=False).to_csv(path+"jdbymodelcount.csv", index=False)
    #print(datagrouptitle.sort_values(by='count', ascending=False))
#readjdbymodel()