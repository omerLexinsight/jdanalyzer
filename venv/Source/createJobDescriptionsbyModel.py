import pandas as pd

import json
import pickle
import re
import keras
import numpy as np
import pandas as pd
from ExcelOperations import *

global tokenizer, classnames, model, separatorspattern, multispacepattern

path = "/mnt/usb-ssd/csvfiles/"
#path = "/Users/omerorhan/jdanalyzer/gloves/"
#excelfile = path + "salesdata/sales.xlsx"
excelfile = "sales.xlsx"

# 2-Environment setting(Server:S, Local:L)


def isNaN(num):
    return num != num


# reads job fuctions from excel file based on google excel. Before run the function download excel and change the path.
def readjobfunctionsbythebalancecareers():
    diclist = {}
    mustvalues = ""
    datafunctionsprev = ""
    # reads sub functions until find empty rows. If it finds empty rows then looking for job titles.
    for x in range(1, 100):
        datafunctions = readExcelRange(excelfile, "Sales", x, x, 1, 1)
        if str(datafunctions) == "[]" or str(datafunctions[0][0]) == "END":
            break
        if str(datafunctions[0][0]) == "nan":
            # it is for job titles.
            datamust = readExcelRange(excelfile, "Sales", x, x, 2, 2)
            if not str(datamust) == '[]' and not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        else:
            datafunctionsprev = datafunctions
        # understands for new subfunction
        if readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1) != "nan" and str(
                readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1)[0][0]) != "nan":
            if len(mustvalues) > 0 and mustvalues[-1] == "|":
                mustvalues = mustvalues[0:-1]
            diclist.update({str(datafunctionsprev[0][0]): mustvalues})
            mustvalues = ""
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    return diclist


listoftitles = readjobfunctionsbythebalancecareers()
print(listoftitles)
knownlabels = []  # Index into this to get string label for any numeric tag
labels_index = {}  # dict from string label to numeric tag
labelset = set()  # Used to ensure unique labels while reading file


def reallyclean(s):
    breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
    unwantedseparators = re.compile(
        r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
    markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
    multisemipattern = re.compile(r"( *; *)+")
    multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n

    if type(s) is str:
        s = " " + s.lower() + " "
    else:
        s = " " + s + " "

    # s = " " + s.lower() except AttributeError: pass + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s


with open(path + 'tokenizer.pickle',
          'rb') as handle:  # Must use a tokenizer with the same dictionary we used for learning
    tokenizer = pickle.load(handle)  # So we pickled what we learned with and then unpickle it here
with open(path + 'classnames.json', 'r') as classnames_file:
    knownlabels = json.load(classnames_file)  # Load translations from our class indices to practice area names
print(knownlabels)
model = keras.models.load_model(path + "model.h5",
                                compile=False)  # Load Keras model we learned using KerasPractice notebook
print("Loaded model")

for label in knownlabels:
    tag = len(labels_index)
    labels_index[label] = tag  # Construct dict from label (string) to tag (numerical)


# 5-practicearea; predict sector names
def practicearea(text):  # Returns practice are given any text.
    global tokenizer, classnames, model
    sequences = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=1000)
    prediction = model.predict(sequences)  # Run the actual classification
    return (list(knownlabels.keys())[list(knownlabels.values()).index(np.argmax(prediction))])


# 3-Read and prepare job description from Sample file for prediction
def make_label_tensor(doclabels, allknownlabels):
    tensor = np.zeros(len(allknownlabels)).astype(np.float32)
    for i in range(len(allknownlabels)):
        if allknownlabels[i] in doclabels:
            tensor[i] = 1.0
    return tensor


# texts = open(path + 'sample', 'r') #sampe is just a text file. You can copy your job description in it.
dataset = pd.read_csv(path+"SalesjD.csv", index_col=False)

# print(dataset.shape)
def createjdbymodel(data):
    df = pd.DataFrame(columns=list(['Label', 'Title', 'Description']))
    print("started prediction")
    count = 0
    for index, row in data.iterrows():
        count = count + 1
        text = reallyclean(row['Description'])
        prediction = practicearea(text)
        for key in listoftitles.keys():
            if prediction == key:
                df = df.append({'Label': key, 'Title': row["Title"], 'Description': row["Description"]},
                               ignore_index=True)
        if count % 1000 == 0:
            print(str(count))
    df.to_csv(path + "jdbymodel.csv", index=False)


createjdbymodel(dataset)


def createtrainingfile():
    def createjdbymodel(data):
        df = pd.DataFrame(columns=list(['Label', 'Title', 'Description']))
        print("started prediction")
        count = 0
        for index, row in data.iterrows():
            count = count + 1
            text = reallyclean(row['Description'])
            prediction = practicearea(text)
            for key in listoftitles.keys():
                if prediction == key:
                    df = df.append({'Label': key, 'Title': row["Title"], 'Description': row["Description"]},
                                   ignore_index=True)
            if count % 1000 == 0:
                print("predicton:" + str(count) + "/" + str(data.shape[0]))
            return df

    def chunck_generator(filename, header=False, chunk_size=10 ** 5):
        for chunk in pd.read_csv(filename, delimiter=',', chunksize=chunk_size, iterator=True, parse_dates=[1],
                                 engine='python',
                                 index_col=False):
            yield (chunk)

    def generator(filename, header=False, chunk_size=10 ** 5):
        chunk = chunck_generator(filename, header=False, chunk_size=10 ** 5)
        for row in chunk:
            yield row

    filename = path + "SalesjD.csv"
    generator = generator(filename=filename)
    count = 1
    listresults = []
    while True:
        try:
            listresults.append(createjdbymodel(next(generator)))
            count = count + 1
            print("chunk count;" + str(count))
        except StopIteration:
            break
    print("sss")
    pd.concat(listresults).to_csv(path + "jdbymodel.csv", index=False)

#createtrainingfile()


# dataset['Title'].str.lower().str.contains(value, na=False)== False)
