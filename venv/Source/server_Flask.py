'''
#this file is for server flask integration

You may need flask configuration settings. Look at this page;
https://www.analyticsvidhya.com/blog/2017/09/machine-learning-models-as-apis-using-flask/


    In server_FLask.py file;
    1-Get a request
    2-Send for prediction to predictionarea
    3-Read our model data from tokenizer.pickle, classnames.json and model.h5
    4-Clean and prepare job description
    5-Make prediction
'''

"""Filename: server.py
to start server you should use below code
gunicorn --bind 0.0.0.0:8000 server:app
"""
import pandas as pd
from flask import Flask, jsonify, request
import pickle, json, keras
import numpy as np
import re
import html2text

app = Flask(__name__)

knownlabels = []  # Index into this to get string label for any numeric tag
labels_index = {}  # dict from string label to numeric tag
labelset = set()  # Used to ensure unique labels while reading file

path = '/Users/omerorhan/jdanalyzer/gloves/serverFiles/salesdata/'

global tokenizer, classnames, model, separatorspattern, multispacepattern


def reallyclean(s):
    h = html2text.HTML2Text()
    h.ignore_links = True
    s = h.handle(s)
    breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
    unwantedseparators = re.compile(
        r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
    markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
    multisemipattern = re.compile(r"( *; *)+")
    multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n

    s = " " + s.lower() + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s


def predictionarea(text):  # Returns practice are given any text.
    # 3-Read our model data from tokenizer.pickle, classnames.json and model.h5
    with open(path + 'tokenizer.pickle',
              'rb') as handle:  # Must use a tokenizer with the same dictionary we used for learning
        tokenizer = pickle.load(handle)  # So we pickled what we learned with and then unpickle it here
    with open(path + 'classnames.json', 'r') as classnames_file:
        knownlabels = json.load(classnames_file)  # Load translations from our class indices to practice area names
    model = keras.models.load_model(path + "model.h5")  # Load Keras model we learned using KerasPractice notebook

    # 4-Clean and prepare job description
    text = reallyclean(text)
    sequences = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences([text]),
                                                           maxlen=1000)
    # 5-Make prediction
    prediction = model.predict(sequences)
    return (list(knownlabels.keys())[list(knownlabels.values()).index(np.argmax(prediction))])


@app.route('/predict', methods=['POST'])
def apicall():
    # 1-Get a request
    try:
        test_json = request.get_json()  # all communication in JSON format
        test = pd.read_json(test_json, orient='records')  # reading jSON text
        test = test['text']  # getting text
        for key, value in test.items():
            test = value
    except Exception as e:
        raise e
    if test is None:  # empty text control
        return 'error:text cannot be empty'
    else:
        # 2-Send text for a prediction to predictionarea
        final_predictions = pd.DataFrame([predictionarea(test)])
        responses = jsonify(predictions=final_predictions.to_json(orient="records"))
        responses.status_code = 200
        return (responses)
