'''This script loads pre-trained word embeddings (GloVe embeddings)
into a frozen Keras Embedding layer, and uses it to
train a text classification model on the 20 Newsgroup dataset
(classification of newsgroup messages into 20 different categories).
GloVe embedding data can be found at:
http://nlp.stanford.edu/data/glove.6B.zip
(source page: http://nlp.stanford.edu/projects/glove/)
20 Newsgroup data can be found at:
http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-20/www/data/news20.html

https://github.com/keras-team/keras/blob/master/examples/pretrained_word_embeddings.py
'''

from __future__ import print_function

import keras
import os
import pickle

import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, CSVLogger
from keras.initializers import Constant
from keras.layers import Conv1D, MaxPooling1D, Embedding, Dropout
from keras.layers import Dense, Input, GlobalMaxPooling1D
from keras.models import Model
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical
import os
import log4csv as log4csv
import datetime

# https://stackoverflow.com/questions/50591982/importerror-cannot-import-name-timestamp
# code to execute the model on GPU
# import os

environment = 'L'

if environment == 'S':
    BASE_DIR = './'
    resultsdir = './store/'
    os.environ["CUDA_VISIBLE_DEVICES"] = '1'
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    keras.backend.set_session(sess)
elif environment == 'L':
    BASE_DIR = '/Users/omerorhan/jdanalyzer/gloves/'
    resultsdir = '/Users/omerorhan/jdanalyzer/gloves/'

# GPU code ends here
parameterdic = {"processdate": str(datetime.datetime.now())}
"""
   parameterdic = {"processdate": '1', "word_vectors_count": '2', "text_size": '1', "data_shape": '1', "model_loss": '1',
                    "model_optimizer": '1', "model_metrics": '1', "early_stop_patience": '1', "fit_epochs": '1',
                    "fit_batch_size": '1',
                    "result_loss": '1',
                    "result_val_loss": '1', "result_categorical_accuracy": '1', "result_val_categorical_accuracy": '1'}
"""

GLOVE_DIR = os.path.join(BASE_DIR, 'glove.6B')
TEXT_DATA_DIR = os.path.join(BASE_DIR, '20_newsgroups')
MAX_SEQUENCE_LENGTH = 1000
MAX_NUM_WORDS = 20000
EMBEDDING_DIM = 200
VALIDATION_SPLIT = 0.2

# first, build index mapping words in the embeddings set
# to their embedding vector

print('Indexing word vectors.')

embeddings_index = {}
with open(os.path.join(GLOVE_DIR, 'glove.6B.200d.txt')) as f:
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs

print('Found %s word vectors.' % len(embeddings_index))
parameterdic.update({"word_vectors_count": str(len(embeddings_index))})
# second, prepare text samples and their labels
print('Processing text dataset')

dataset = pd.read_csv(BASE_DIR + "traindata2.csv", engine='python', index_col=False)

texts = []  # list of text samples
labels_index = {}  # dictionary mapping label name to numeric id
labels = []  # list of label ids

listOfSectors = {"Sales": 0, "Customer Service": 1,
                 "Software": 2,
                 "Legal": 3, "Marketing": 4,
                 "Finance": 5, "Accounting": 6, "Treasury": 7}
labels_index = listOfSectors

for index, row in dataset.iterrows():
    # label_id = len(labels_index)
    texts.append(row['Description'])
    labels.append(listOfSectors.get(row['Label']))

import json

with open(resultsdir + 'classnames.json',
          'w') as classnames_file:  # Save word_index separately so we can use it in runtime classifier
    json.dump(labels_index, classnames_file)

print('Found %s texts.' % len(texts))
parameterdic.update({"text_size": str(len(texts))})

# finally, vectorize the text samples into a 2D integer tensor
tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
print('Starting fit_on_texts')
tokenizer.fit_on_texts(texts)
print('Starting texts_to_sequences')
sequences = tokenizer.texts_to_sequences(texts)

word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

labels = to_categorical(np.asarray(labels))
parameterdic.update({"data_shape": str(data.shape)})
print('Shape of data tensor:', data.shape)
print('Shape of label tensor:', labels.shape)

# split the data into a training set and a validation set
indices = np.arange(data.shape[0])
np.random.shuffle(indices)
data = data[indices]
labels = labels[indices]
num_validation_samples = int(VALIDATION_SPLIT * data.shape[0])

x_train = data[:-num_validation_samples]
y_train = labels[:-num_validation_samples]
x_val = data[-num_validation_samples:]
y_val = labels[-num_validation_samples:]

print('Preparing embedding matrix.')

# prepare embedding matrix
num_words = min(MAX_NUM_WORDS, len(word_index)) + 1
embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))
for word, i in word_index.items():
    if i > MAX_NUM_WORDS:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

# load pre-trained word embeddings into an Embedding layer
# note that we set trainable = False so as to keep the embeddings fixed
embedding_layer = Embedding(num_words,
                            EMBEDDING_DIM,
                            embeddings_initializer=Constant(embedding_matrix),
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)

print('Training model.')

# train a 1D convnet with global maxpooling
sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
embedded_sequences = embedding_layer(sequence_input)
x = Conv1D(128, 5, activation='relu')(embedded_sequences)
x = Dropout(0.5)(x)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = GlobalMaxPooling1D()(x)
x = Dense(128, activation='relu')(x)
preds = Dense(len(labels_index), activation='sigmoid')(x)

model = Model(sequence_input, preds)

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
parameterdic.update({"model_loss": str(model.loss)})
parameterdic.update({"model_optimizer": str(model.optimizer)})
parameterdic.update({"model_metrics": str(model.metrics)})
early_stop = EarlyStopping(monitor='val_categorical_accuracy', patience=10, mode='max')
csv_logger = CSVLogger(resultsdir + 'top_model_training.csv')
history = model.fit(x_train, y_train,
                    batch_size=128,
                    epochs=2,
                    validation_data=(x_val, y_val),
                    callbacks=[early_stop]
                    )
parameterdic.update({"early_stop_patience": str(early_stop.patience)})
print(csv_logger)
# create plot
# dict_keys(['val_loss', 'val_categorical_accuracy', 'loss', 'categorical_accuracy'])
import matplotlib.pyplot as plt

history_dict = history.history
print(history_dict.keys())
loss_values = history_dict['loss']
val_loss_values = history_dict['val_loss']
acc = history_dict['categorical_accuracy']
valacc = history_dict['val_categorical_accuracy']

epochs = range(1, len(acc) + 1)
parameterdic.update({"fit_epochs": str(epochs)})
parameterdic.update({"result_loss": str(loss_values)})
parameterdic.update({"result_val_loss": str(val_loss_values)})
parameterdic.update({"result_categorical_accuracy": str(acc)})
parameterdic.update({"result_val_categorical_accuracy": str(valacc)})

plt.subplot(221)
plt.plot(epochs, loss_values, 'bo', label='Training loss')
plt.plot(epochs, val_loss_values, 'g', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.savefig(resultsdir + "Loss.png")

plt.subplot(222)
plt.plot(epochs, valacc, 'r', label='val_categorical_accuracy')
plt.plot(epochs, acc, 'y', label='categorical_accuracy')
plt.title('Val Cat. and Cat. Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.savefig(resultsdir + "Val.png")

plt.show()
# save model
print(model.summary())
print("Saving model to disk...")
keras.models.save_model(model, resultsdir + "model.h5")  # Save almost everything in one h5 file. What is missing:
with open(resultsdir + 'dictionary.json',
          'w') as dictionary_file:  # Save word_index separately so we can use it in runtime classifier
    json.dump(word_index, dictionary_file)
with open(resultsdir + 'tokenizer.pickle', 'wb') as tokenizer_file:  # Save entire tokenizer separately
    pickle.dump(tokenizer, tokenizer_file, protocol=pickle.HIGHEST_PROTOCOL)
# os.rename("code.log", resultsdir + "code.log")     # Move our code hyperparameter dump (made by bin/logexp script) to resultsdir
# os.rename("KerasPractice.py", resultsdir + "KerasPractice.py")        # Might as well copy all the code we used
print("Saved model and code to disk in directory " + resultsdir)

log4csv.savelog4csv(resultsdir, parameterdic)
