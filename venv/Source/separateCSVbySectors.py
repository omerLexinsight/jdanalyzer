'''
This file writes separated CSV files by sector from the main CSV file
First, it reads job descriptions title and searches keyword for separation
my recommendation is using Python script in command line due to performance efficiency

In file;
    1-Read main CSV file
    2-Search each sector value in Title
    3-Append each row in a list
    4-Write CSV file in a disk
'''
# import libraries
import csv
import sys
import pandas as pd
from ExcelOperations import *

csv.field_size_limit(
    sys.maxsize)  # this size limit is very important, because this program is designed for more than 10 million job description.

# 1-Read main CSV file
# path = "/mnt/usb-ssd/csvfiles/"
path = "/Users/omerorhan/jdanalyzer/gloves/"

fields = ['Title', 'Description']
dataset = pd.read_csv(path + "combined_csv.csv", engine='python', usecols=fields,
                      index_col=False)  # reading main CSV file.
excelfile = path + "functions& industries_list.xlsx"


# dtTitle = dataset['Title']


def isNaN(num):
    return num != num


def readfunctionexcelincluding():
    nrow = 1
    diclist = {}
    for x in range(2, 28):
        datafunctions = readExcelRange(excelfile, "Functions", x, x, 1, 1)
        if str(datafunctions[0][0]) == "nan":
            continue
        mustvalues = ""
        for y in range(3, 10):
            datamust = readExcelRange(excelfile, "Functions", x, x, y, y)
            if not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        if len(mustvalues) > 0 and mustvalues[-1] == "|":
            mustvalues = mustvalues[0:-1]
        diclist.update({datafunctions[0][0]: mustvalues})
    return diclist


def readfunctionexcelnotincluding():
    nrow = 1
    diclist = {}
    for x in range(2, 28):
        datafunctions = readExcelRange(excelfile, "Functions", x, x, 1, 1)
        mustvalues = ""
        for y in range(10, 13):
            datamust = readExcelRange(excelfile, "Functions", x, x, y, y)
            if not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        if len(mustvalues) > 0 and mustvalues[-1] == "|":
            mustvalues = mustvalues[0:-1]
        diclist.update({datafunctions[0][0]: mustvalues})
    return diclist


keywordsIncluded = {}
keywordsNotIncluded = {}


# keywordsIncluded = readfunctionexcelincluding()
# keywordsNotIncluded = readfunctionexcelnotincluding()
# keywordsNotIncluded = {str(k).lower(): str(v).lower() for k, v in keywordsNotIncluded.items()}
# keywordsIncluded = {str(k).lower(): str(v).lower() for k, v in keywordsIncluded.items()}


def separatedSectorJD(listOfSectors, dataset):
    labelsectorlist = []
    for key, value in keywordsIncluded.items():
        # print(dataset['Title'].str.lower().str.contains(value, na=False)== False)
        # print(dataset['Title'].apply(str.lower).str.contains(value, na=False))
        # 2-Search each sector value in Title
        notinclude = keywordsNotIncluded.get(key)
        if (notinclude != ""):
            salesdt = dataset[
                dataset['Title'].str.lower().str.contains(value, na=False) & (
                        dataset['Title'].str.lower().str.contains(notinclude,
                                                                  na=False) == False)]  # searching each sector value in Title
        else:
            salesdt = dataset[
                dataset['Title'].str.lower().str.contains(value, na=False)]  # searching each sector value in Title
        salesdt['Label'] = key
        key = key.replace(" ", "_")
        salesdt.to_csv(path + "separated/" + key + "jD.csv", index=False)
        # 3-Append each row in a list
        # Write CSV file in a disk
        # salesdt.to_csv("/mnt/usb-ssd/csvfiles/separated/" + key + "jD.csv", index=False)


listOfSectors = {"Sales": "Sales|Account Executive", "Customer_Service": "Customer Service|Customer Success",
                 "Software": "Software",
                 "Legal": "Attorney", "Marketing": "Marketing",
                 "Finance": "Investment|Finance", "Accounting": "Accountant|Controller", "Treasury": "Treasury",
                 "Operations_Management": "Supply Chain|Operations",
                 "Product_Management": "Product Owner|Product Manager"}

listOfSectors = {"Operations Management": "Supply Chain|Operations"}


# Operation management key words
# jdlabel = separatedSectorJD(keywordsIncluded, dataset)


# jdlabel.to_csv("/mnt/usb-ssd/csvfiles/salesjD.csv", index=False)

# dataset = pd.read_csv("/mnt/usb-ssd/csvfiles/separated/combined_csv.csv", engine='python',
#                      index_col=False)


def separatesummary():
    import glob

    files = glob.glob("/mnt/usb-ssd/csvfiles/separated/" + "*.csv")  # picking up XML files in folder

    for item in files:
        dataset = pd.read_csv(item, engine='python',
                              index_col=False)
        print(item + '-' + str(dataset.shape))


# data = readExcelRange(excelfile, "Functions", 2, 999, 1, 1)
# print(data)


# https://www.thebalancecareers.com/job-titles-a-z-list-2061557
def readjobfunctionsbythebalancecareers():
    # excelfile = "/Users/omerorhan/Desktop/sales.xlsx"
    excelfile = path + "sales.xlsx"
    diclist = {}
    for x in range(1, 6):
        datafunctions = readExcelRange(excelfile, "Sales", x, x, 1, 1)
        if str(datafunctions[0][0]) == "nan":
            continue
        mustvalues = ""
        for y in range(2, 30):
            datamust = readExcelRange(excelfile, "Sales", x, x, y, y)
            if not str(datamust) == '[]' and not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        if len(mustvalues) > 0 and mustvalues[-1] == "|":
            mustvalues = mustvalues[0:-1]
        diclist.update({datafunctions[0][0]: mustvalues})
    return diclist


def separatedSectorJDthebalancecareers(listOfSectors, data):
    dataset = data
    listOfSectors = {str(k).lower(): str(v).lower() for k, v in listOfSectors.items()}
    listOfSectors = {str(k).lower(): str(v).lower() for k, v in listOfSectors.items()}
    listofdataset = []
    for key, value in listOfSectors.items():
        salesdt = dataset[dataset['Title'].str.lower().isin(value.split("|"))]
        salesdt['Label'] = key
        listofdataset.append(salesdt)
    salesdt = pd.concat(listofdataset, ignore_index=True)
    return salesdt

listoftitles = readjobfunctionsbythebalancecareers()

#countsdic, countsdicdetailed = (separatedSectorJDthebalancecareers(listoftitles, dataset))

#pd.read_csv(path + "combined_csv.csv", engine='python', usecols=fields,
#                      index_col=False, chunksize=chunksize)  # reading main CSV file.

#for chunk in pd.read_csv(path + "combined_csv.csv", engine='python', usecols=fields,
#                      index_col=False, chunksize=chunksize):
#    pd.concat(separatedSectorJDthebalancecareers(chunk))

chunksize = 10 ** 8

dataframe = pd.concat([separatedSectorJDthebalancecareers(listoftitles, chunk) for chunk in (pd.read_csv(path + "combined_csv.csv", engine='python',
                      index_col=False, chunksize=chunksize))])

dataframe.to_csv(path + "separated/trainingjD.csv", index=False)

# countsdic = pd.DataFrame.from_dict(countsdic, orient='index',
#                                   columns=['function', 'frequency'])
# print(countsdic)

# salesdt = dataset[
#   dataset['Title'].str.lower() == "account representative"]  # searching each sector value in Title

# print(salesdt.shape)
