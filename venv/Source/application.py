# https://www.youtube.com/watch?v=P5kGTr2zfn4&t=100s
"""
export FLASK_APP=application.py
export FLASK_DEBUG=1
flask run
"""
# !/usr/bin/python3
from flask_restful import Resource, Api, reqparse
from flask import Flask, jsonify, request
import pickle, json, keras
import numpy as np
import re
from keras import backend as K
import keras_preprocessing


application = Flask(__name__)
api = Api(application)

knownlabels = []  # Index into this to get string label for any numeric tag
labels_index = {}  # dict from string label to numeric tag
labelset = set()  # Used to ensure unique labels while reading file

global tokenizer, classnames, model, separatorspattern, multispacepattern
environment = 'L'  # environment L: Local  S:Server Server is for company's GPU

if environment == 'L':
    path = '/Users/omerorhan/jdanalyzer/gloves/salesdata/'
else:
    path = ""


def reallyclean(s):
    breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
    unwantedseparators = re.compile(
        r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
    markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
    multisemipattern = re.compile(r"( *; *)+")
    multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n
    s = " " + s.lower() + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s


def predictionarea(text):  # Returns practice are given any text.
    # 3-Read our model data from tokenizer.pickle, classnames.json and model.h5
    # Must use a tokenizer with the same dictionary we used for learning
    with open(path + 'tokenizer.pickle',
              'rb') as handle:  # Must use a tokenizer with the same dictionary we used for learning
        tokenizer = pickle.load(handle)
    with open(path + 'classnames.json', 'r') as classnames_file:
        knownlabels = json.load(classnames_file)  # Load translations from our class indices to practice area names
    model = keras.models.load_model(path + "model.h5")  # Load Keras model we learned using KerasPractice notebook
    # 4-Clean and prepare job description
    text = reallyclean(text)
    sequences = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences([text]),
                                                           maxlen=1000)
    # 5-Make prediction
    prediction = model.predict(sequences)
    return (list(knownlabels.keys())[list(knownlabels.values()).index(np.argmax(prediction))])


class jdclassifier(Resource):
    def get(self):
        try:
            test = request.form['text']
        except Exception as e:
            raise e
        if test is None:  # empty text control
            return 'error:text cannot be empty'
        else:
            # 2-Send text for a prediction to predictionarea
            responses = jsonify(Sector=predictionarea(test))
            responses.status_code = 200
            K.clear_session()
            return responses


api.add_resource(jdclassifier, '/predict', endpoint='bar')

if __name__ == "__main__":
    application.run(debug=True)
