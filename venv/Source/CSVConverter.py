'''
Reading different types of job descriptions such as XML and JSON then saving in CSV format.

    #list of companies;
    # 1 career_builder_jobs.csv -XML
    # 2 usajobs-job_deduped.csv -XML
    # 3 dice_com-job_us.csv.csv - XML
    # 4 indeed_com-jobs_us.csv- XML
    # 5 monster_com-job.csv - XML
    # 6 jobs_monster-job_deduped_n_merged_20180529_101720743264062 - XML
    # 7 Indeed -JSON

important notice:
xml2csv can be imported from project interpreter     pip install xml2csv
when you get 'AttributeError: 'IterParseIterator' object has no attribute 'next'' error
Just change line 57 in xml2csv.py from this: event, root = self.context.next() to this: event, root = self.context.__next__()

'''

#1-Import packages
import glob
import pandas as pd
import csv
import sys

csv.field_size_limit(sys.maxsize) #this size limit is very important, because this program is designed for more than 10 million job description.


def xmltocsv(inputfolderpath, outputpathfolder):
    #This function converts XML file into CSV
    files = glob.glob(inputfolderpath + "*.xml")  #picking up XML files in folder
    count = 0
    dfs = []
    for item in files:
        print("count:" + str(count) + " " + item[:-4] + ' is being processed!')
        print(outputpathfolder + item.split('/')[6][
                                 :-4] + ".csv")
        #xml2csv is a converting library. you may need some configuraion changes, look at the beginning of the file
        converter = xml2csv(item,
                            outputpathfolder + item.split('/')[6][
                                               :-4] + ".csv",
                            encoding="utf-8")
        dfs.append(converter.convert(tag="page"))
        print(item + ' ended!')
        count = count + 1


def mergeCsvFiles(path):
    #this function merges all csv files into one main csv file.
    print('mergeCsvFiles is started')
    files = glob.glob(path + "*.csv") #getting files in folder
    combined_csv = pd.concat([pd.read_csv(f, engine='python', index_col=False) for f in files]) #merging files
    combined_csv.to_csv(path + "combined_csv.csv",
                        index=False)  #write in a disk
    print('mergeCsvFiles is ended')


def JSONtoCSV(inputfolderpath, outputpathfolder):
    #this file prepares a CSV file from JSON file. This code is adjusted for Indeed job descriptions.

    files = glob.glob(inputfolderpath + "*.json") #reading JSON files from folder
    count = 0
    for item in files:  #reading each file in folder
        print("count:" + str(count) + " " + item[:-4] + ' is being processed!')
        print(outputpathfolder + item.split('/')[6][
                                 :-4] + "csv")
        data = pd.read_json(item, typ='frame')
        data = data['root']
        data = data['page']
        df = pd.DataFrame()
        list1 = []
        for row in data:
            try:
                if row['record']['uniq_id'] is not None:
                    unique_id = row['record']['uniq_id']
                else:
                    unique_id = ' '
            except KeyError:
                unique_id = ' '
            try:
                if row['record']['sub_domain'] is not None:
                    sub_domain = row['record']['sub_domain']
                else:
                    sub_domain = ' '
            except KeyError:
                sub_domain = ' '
            try:
                if row['record']['title'] is not None:
                    title = row['record']['title']
                else:
                    title = ' '
            except KeyError:
                title = ' '
            try:
                if row['record']['location'] is not None:
                    location = row['record']['location']
                else:
                    location = ' '
            except KeyError:
                location = ' '
            try:
                if row['record']['description'] is not None:
                    description = row['record']['description']
                else:
                    description = ' '
            except KeyError:
                description = ' '
            try:
                if row['record']['direct_link_to_the_job_page'] is not None:
                    direct_link_to_the_job_page = row['record']['direct_link_to_the_job_page']
                else:
                    direct_link_to_the_job_page = ' '
            except KeyError:
                direct_link_to_the_job_page = ' '
            try:
                if row['record']['direct_apply_link_on_the_job_page'] is not None:
                    direct_apply_link_on_the_job_page = row['record']['direct_apply_link_on_the_job_page']
                else:
                    direct_apply_link_on_the_job_page = ' '
            except KeyError:
                direct_apply_link_on_the_job_page = ' '
            try:
                if row['record']['postedAtText'] is not None:
                    postedAtText = row['record']['postedAtText']
                else:
                    postedAtText = ' '
            except KeyError:
                postedAtText = ' '
            try:
                if row['record']['postedAt'] is not None:
                    postedAt = row['record']['postedAt']
                else:
                    postedAt = ' '
            except KeyError:
                postedAt = ' '
            try:
                if row['record']['company_name'] is not None:
                    company_name = row['record']['company_name']
                else:
                    company_name = ' '
            except KeyError:
                company_name = ' '
            #merge in dataframe
            list1.append(
                unique_id + '{#~#}' + sub_domain + '{#~#}' + title + '{#~#}' + location + '{#~#}' + description + '{#~#}'
                + direct_link_to_the_job_page + '{#~#}' + direct_apply_link_on_the_job_page
                + '{#~#}' + postedAtText + '{#~#}' + postedAt + '{#~#}' + company_name)

        df = pd.DataFrame(list1)
        df = df[0].str.split('{#~#}', expand=True)
        df.columns = ['unique_id', 'sub_domain', 'title', 'location', 'description', 'direct_link_to_the_job_page',
                      'direct_apply_link_on_the_job_page', 'postedAtText', 'postedAt', 'company_name']#setting column names
        df.to_csv(outputpathfolder + item.split('/')[6][
                                     :-4] + "csv", index=False) #writing in csv file
        print(item + ' ended!')
        count = count + 1


def mergeCompaniesCSV(folderpath):
    #this function is special for each company's job description.
    #There are 6 CSV files from different companies.

    #list of companies;
    # 1 career_builder_jobs.csv
    # 2 usajobs-job_deduped.csv
    # 3 dice_com-job_us.csv.csv
    # 4 indeed_com-jobs_us.csv
    # 5 monster_com-job.csv
    # 6 jobs_monster-job_deduped_n_merged_20180529_101720743264062
    import datetime
    now = datetime.datetime.now()

    # 1 career_builder_jobs.csv

    '''
    print('career_builder_jobs is being proccessed ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    career_builder_dt = pd.read_csv(folderpath + "career_builder_jobs.csv",
                                    engine='python', index_col=False)
    career_builder_dt = career_builder_dt[
        ['record_jobtitle', 'record_jobdescription', 'record_company', 'record_industry']]
    career_builder_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print('processing career_builder_jobs is ended ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    '''
    # 2 usajobs-job_deduped.csv done
    '''
    print('usajobsjob_dt is being processed ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    usajobsjob_dt = pd.read_csv(folderpath + "usajobs-job_deduped_n_merged_20180529_101958922661692.csv",
                                engine='python', index_col=False)
    # print(usajobsjob_dt.columns.values)
    usajobsjob_dt = usajobsjob_dt[
        ['record_job_title', 'record_job_description', 'record_organization', 'record_sector']]
    usajobsjob_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print(
        'usajobsjob_dt is being ended ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))
    '''
    # print(usajobsjob_dt)

    # 3 dice_com-job_us.csv.csv done
    '''
    print('dicecomjobus_dt is started ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    dicecomjobus_dt = pd.read_csv(folderpath + "dice_com-job_us.csv",
                                  engine='python', index_col=False)
    dicecomjobus_dt = dicecomjobus_dt[
        ['record_jobtitle', 'record_jobdescription', 'record_company']]
    dicecomjobus_dt['Sector'] = ''
    dicecomjobus_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print('dicecomjobus_dt is ended ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    '''
    # 4 indeed_com-jobs_us.csv
    print('indeedcom_dt is started ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))

    indeedcom_dt = pd.read_csv(folderpath + "indeed_com-jobs.csv",
                               engine='python', index_col=False)
    indeedcom_dt = indeedcom_dt[
        ['title', 'description', 'company_name']]
    indeedcom_dt['Sector'] = ''
    indeedcom_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print('indeedcom_dt is ended ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))

    # 5 monster_com-job.csv
    print('monster_com-job is started ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))
    monster_com_dt = pd.read_csv(folderpath + "monster_com-job.csv",
                                 engine='python', index_col=False)
    monster_com_dt = monster_com_dt[
        ['record_job_title', 'record_job_description', 'record_organization', 'record_sector']]
    monster_com_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print('monster_com-job is ended ' + str(datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))

    # 6 jobs_monster-job_deduped_n_merged_20180529_101720743264062
    print('jobs_monster-job_deduped_n_merged_20180529_10172074326406 is started ' + str(
        datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))

    jobs_monster_job_dt = pd.read_csv(folderpath + "jobs_monster-job_deduped_n_merged_20180529_101720743264062.csv",
                                      engine='python', index_col=False)
    # print(jobs_monster_job_dt.columns.values)
    jobs_monster_job_dt = jobs_monster_job_dt[
        ['record_job_title', 'record_job_description', 'record_sector']]
    jobs_monster_job_dt['Organization'] = ''
    jobs_monster_job_dt.columns = ['Title', 'Description', 'Organization', 'Sector']
    print('jobs_monster-job_deduped_n_merged_20180529_10172074326406 is started ' + str(
        datetime.datetime.now().hour) + ':' + str(
        datetime.datetime.now().minute))

    print('combined_dt is started ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))
    combined_dt = pd.read_csv(folderpath + "combined_csv.csv",
                              engine='python', index_col=False)
    print('combined_dt is ended ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))

    print('pd.concat is started ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))
    combined_csv = pd.concat([combined_dt, indeedcom_dt, monster_com_dt, jobs_monster_job_dt])
    print('pd.concat is ended ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))

    print('to_csv is started ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))
    combined_csv.to_csv(folderpath + "combined_csv2.csv",
                        index=False)
    print('to_csv is finished ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))

###################################


#this function merges all csv files into one main csv file.
# mergeCompaniesCSV('/Volumes/Elements/jobs-data/csv files/')

#This function converts XML file into CSV
# xmltocsv("/Users/omerorhan/Desktop/Job Description Analyzer/xml_files/",
#     "/Users/omerorhan/Desktop/Job Description Analyzer/csv_files/")

#Merge many CSV files into one main CSV file
# mergeCsvFiles("/Users/omerorhan/Desktop/Job Description Analyzer/csv_files/")

#This function converts JSON file into CSV
# JSONtoCSV("/Users/omerorhan/Desktop/Job Description Analyzer/json_files/",
#         "/Users/omerorhan/Desktop/Job Description Analyzer/csv_files/")





