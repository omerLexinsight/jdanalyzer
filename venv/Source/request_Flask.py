'''
    this file is for request a flask prediction
    you have two options either reading from text or just using a job description link

    In File;
    1-Read job descriptions from a file or a link
    2-Send a request to Server_Flask
    3-Print a result (response is in JSON format)
'''
import requests
import pandas as pd
import json
import ssl


def callFlaskService():
    header = {'Content-Type': 'application/json', \
              'Accept': 'application/json'}
    path = '/Users/omerorhan/jdanalyzer/gloves/'

    #    1-Read job descriptions from a file or a link
    # read from link
    resp = None
    ssl._create_default_https_context = ssl._create_unverified_context  # most of links are SSL. It is for configuration.
    link = "https://www.indeed.com/pagead/clk?mo=r&ad=-6NYlbfkN0AMJeVuk4ECd5K_1LfpdW7JxefdFJh_RJqhU5XMhpZ8Eclou_QC5s4tN8uFb6pfVU18WkpmqB7INxI4OGBnn17ChzwKOmO0RWSrH2UKYnhCA8Q9fhiPrI3UmSUp81xJrD2CIAVlmjcn37PCokpgCjlWYAfLWHakmDF1L4bZb4KNL4J5qf5b04x3VUGZujqpmRr5H1TghqcqGH8EHO7pTvI4h6NlbR4Losg9bhdWLLAKA-y_EjB0gM4QS4JxAeh1H-MCQhpA8I_1s8c1B_TlxZwqTZalIOgXUiMdg61PLO-C1fnRCYJjJffqujOcvZ2os3QhR92Z-Q4oNUjNJJDzJBimQHVFRdGvMgM0oZaXgZtHjvYMuxT4cTueX1Nr7oJU90axCmo3R6Rl_MsOf0MSkkhbY6S-Ou68W8qIVkQmZ1Y6ubYZ0gk0hUemzVgI91_ki1nNQmRtyrmaHz4vOOUf9sD_erMlH1hFk2ltHfhxSqgGKqf5vJbCbRS79nRVHDtypLi7h_4ZVvH1oCTvpK7c6CzruWaaMRJJXkaOnQKefizSvIp9bXx8HCo_zD9B3oT8yjMLkbGM6mXP6skqecE2X9GC1DZH-YjYByss7Wx3ZFyo-q2wnRykQuY6HP_VaQqKoaXcsoGsSppc_kSFW1fZfH6HxrDHPSTrUdgaR4q_seQ0UPTw7oOzPEVs2qoTgl_sycGDl3vinOxRaES80tgtgaARil-Z6PF3azZlPjFht8SGQ5AQA7o__UoXPrwW2mjHybvNlIL5OFg4u2CNKpFG_vcHLgky_-WbBgZ55Tii2d6y7ZTZx6a5xhmM1P4hPNJ-R1fnm4hsbRhdayk1_D_GgYsxU9Bx3urFOGxt4_9oPtyn9hkZsxhYxZqYsjQ2SZHCe_0Dox6p87VL_bAf3Cs4wmevL4FQV5AUpmwg_goHh6qLoFplC3q0J-14&vjs=3&p=1&sk=&fvj=0&tk=1d2dir9951e6h003&jsa=6437&sal=0"
    # with urllib.request.urlopen(link) as url:  #getting source code
    #    print('reading from link')
    #   resp = url.read()

    # read from text
    texts = open(path + 'sample', 'r')
    resp = texts.read()
    print('reading from file')
    texts.close()

    # 2-Send a request to Server_Flask
    dataset = pd.DataFrame([resp], columns=['text'])
    data = dataset.to_json(orient='records')
    # http://0.0.0.0:8000/predict is our server URL
    resp = requests.post("http://34.203.34.15:7999/predict", \
                         data=json.dumps(data), \
                         headers=header)

    # 3-Print a result (response is in JSON format)
    text = json.loads(resp.text)
    print(text['predictions'])


def callAWSservice():
    import requests
    path = '/Users/omerorhan/jdanalyzer/gloves/salesdata/sample'
    texts = open(path, 'r')
    resp = texts.read()
    print('reading from file' + resp)
    texts.close()
    data = json.loads(str(json.dumps({'text': resp})))
    import datetime
    print(datetime.datetime.now())
    resp = requests.get('http://ec2-54-83-67-115.compute-1.amazonaws.com/predict', data=data)
    print(resp.content)
    print(datetime.datetime.now())


callAWSservice()

# Multiclasslabel


'''
system architect
sa1
https://www.indeed.com/viewjob?jk=ea187e89ce6be632&tk=1d3mqjp7qf1tc803&from=serp&vjs=3
sa2
https://www.indeed.com/viewjob?jk=faf3283ff9de1b4c&tk=1d3mquh38b8lt800&from=serp&vjs=3&advn=1175678376742128&adid=66934159&sjdu=yFGnR6TD85yL4qsviYUZxO-bPylNW2vvO3WAHEKN7JgEuyl-uSLk6boycRtdWr-cQSgQUyusSCQzMnkHFBpSY5K1QeS8-lqpby45lHbVUD2uVHnLx-hZgTaPISXzCDw0

system engineer
se1
https://www.indeed.com/viewjob?jk=a79d879b81e77657&tk=1d3mqvo35b8lt800&from=serp&vjs=3&advn=8950287975575301&adid=160408484&sjdu=6ByzYMZLGYUgyrbSdN0cjBkf3WderY8dWs3jhyADUcek7ihpVpburvOg6R_HkLRU7tcm52vYOEzTQd426aEfpjVCANImKkh7dyRvfLztk_oTA6R_kO1Tgt7FOv47Pafpznt6tB7JDVG13vDW7smeig
se2
https://www.indeed.com/cmp/PC-Solutions/jobs/System-Engineer-2272f63c862c9eb2?sjdu=QwrRXKrqZ3CNX5W-O9jEvSACnHMP3BfocKn5vCZ5XEjDWqDbbeaQ9oOj7rGKnTUem38st4N4FUdu-EstVBJnVg&tk=1d3mqve7uf1tc803&adid=230903054&vjs=3
'''
