'''
it prepares training CSV file from the main CSV file. It includes cleaning as well.
My recommendation is using Python script in command line due to performance efficency

    1-Read main CSV file
    2-Get each row separated by sector. (function:combineSectorJD)
    3-Clean text(function:reallyclean)
    4-Merge into training.csv file
'''

import pandas as pd
import csv
import sys
from nltk.util import ngrams
from collections import Counter
from functools import reduce
from collections import OrderedDict
from bs4 import BeautifulSoup
import nltk
import re
import datetime

csv.field_size_limit(sys.maxsize)

# dataset = pd.read_csv("/Users/omerorhan/Desktop/Job Description Analyzer/combined_csv.csv", engine='python',
#                      nrows=100000,
#                      index_col=False)
# datasetTop50Title = dataset['Title'].value_counts()


# print(datasetTop50Title.sum())

# get counts
# sales = (jdlabel[jdlabel['description'].str.contains("Sales|Account Executive")].shape)[0]


dataset = pd.read_csv("/mnt/usb-ssd/csvfiles/combined_csv.csv", engine='python', nrows=10000000, index_col=False)

#2-Get each row separated by sector.
def combineSectorJD(listOfSectors, dataset):
    labelsectorlist = []
    for key, value in listOfSectors.items():  #get sector list
        salesdt = dataset[dataset['Title'].str.contains(value, na=False)] #search in title
        salesdt = salesdt[['Description']]  #set description column
        salesdt['Label'] = key
        labelsectorlist.append(salesdt)  #append a list
    return pd.concat(labelsectorlist)    #merge list

#2-Get each row separated by sector for multi-classification. For example, a job description can have two sectors(Sales-Finance)
def combineSectorJDmulti(listOfSectors, dataset):
    count = 0
    listframes = []
    for index, row in dataset.iterrows():
        label = None
        for key, value in listOfSectors.items():
            if any(x in row['Title'] for x in value.split('|')):
                if label is None:
                    label = key
                else:
                    label = label + '|' + key
        if label is not None:
            #cleans JD
            row['Description'] = reallyclean(
                BeautifulSoup(str(row['Description']).lower(), features="lxml").get_text())
            newdf = pd.DataFrame({"Title": [[row['Title']]], "Label": [label], "Description": [row['Description']]})
            listframes.append(newdf)
        count = count + 1
        if count % 100000 == 0:  #write logs every 100000 JD
            print(str(count) + ' ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute))
    return pd.concat(listframes)


#regex files for cleaning
breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
unwantedseparators = re.compile(
    r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
multisemipattern = re.compile(r"( *; *)+")
multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n

#3-Clean text(function:reallyclean)
def reallyclean(s):
    global separatorspattern, multispacepattern
    s = " " + s.lower() + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s


listOfSectors = {"Sales": "Sales|Account Executive", "Customer Service": "Customer Service|Customer Success",
                 "Software": "Software",
                 "Legal": "Attorney", "Marketing": "Marketing",
                 "Finance": "Investment|Finance", "Accounting": "Accountant|Controller", "Treasury": "Treasury"}

jdlabel = combineSectorJD(listOfSectors, dataset)
# jdlabel = combineSectorJDmulti(listOfSectors, dataset)

jdlabel['Description'] = [reallyclean(BeautifulSoup(str(text).lower(), features="lxml").get_text()) for text in
                          jdlabel['Description']]

#4-Merge into training.csv file
jdlabel.to_csv("/mnt/usb-ssd/csvfiles/traindata3.csv", index=False)


# dataset = pd.read_csv("/Users/omerorhan/Desktop/Job Description Analyzer/traindata2.csv", engine='python',
#                      nrows=100000,
#                      index_col=False)

# dataset['Description Length'] = dataset[dataset['Description'].map(str).apply(len)]

# dataset = dataset[dataset['Description Length'].str.len() > 12]


# print(dataset)

