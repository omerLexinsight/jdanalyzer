from itertools import *
import collections

# --------Generating all four-letter words---------
alphabet = (
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
    'x',
    'y', 'z')
four_letter_words = product(alphabet, alphabet, alphabet, alphabet)

def syllable_count(word):
    word = word.lower()
    count = 0
    vowels = "aeiouy"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if word.endswith("e"):
        count -= 1
    if count == 0:
        count += 1
    return count

liste = []
for word in four_letter_words:
    # --------No consecutively repeating charecters
    letters = (''.join(i for i, _ in groupby(word)))
    # ------No charcter should repeat more than twice
    freq = collections.Counter(letters)
    flag = True
    for k in freq:
        if freq[k] > 2:
            flag = False
    if flag == False:
        continue
    # ----Choose only phono-aestetic words..Read the paper file:///Users/abhiverm/Downloads/English51%20(1).pdf
    # ----Filter out all words that do not contain at least one l and one m.
    if not ("l" in word or "m" in word):
        continue
        # Choose all words thart start with a or i
    if not (letters[0] == "a" or letters[0] == "i"):
        continue
    # Choose all words that contain 2 or 3 syllables
    if not (syllable_count(letters) == 2 or syllable_count(letters) == 3):
        continue
    # Knock out any words that contain at least one u
    if "u" in letters:
        continue
    #double check lenght of words
    if not (len(letters) > 2 and len(letters) < 5):
        continue
    #add to list each perfect word
    liste.append(letters)
#remove duplicates
liste = list(dict.fromkeys(liste))
print(len(liste))
print(liste)
