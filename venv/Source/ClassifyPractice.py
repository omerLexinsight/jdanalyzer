# Prediction file included reading a training file with pickle and making predictions
# This code for testing our model. Flask integration also can be used(look at request and server files.)
'''
In ClassifyPractice.py;
1-Import Libraries
2-Environment setting(Server:S, Local:L)
3-Read and prepare job description from Sample file for prediction
4-Read our trained data from tokenizer.pickle, classnames.json and model.h5
5-practicearea; predict sector names
'''

#1-Import Libraries
import json, pickle, keras ,re , numpy as np
global tokenizer, classnames, model, separatorspattern, multispacepattern

import pandas as pd
#2-Environment setting(Server:S, Local:L)
environment = 'L'
if environment == 'S':
    path = ''
elif environment == 'L':
    path = '/Users/omerorhan/jdanalyzer/gloves/'
def reallyclean(s):
    breakingseparators = re.compile(r"[,\.\"\':;]+")  # Separators we like since they break terms in correct places
    unwantedseparators = re.compile(
        r"[^a-zA-Z0-9&\-,\\;]+")  # A separator is any char that is not alphanumeric (and a few exceptions)
    markupremovalpattern = re.compile(r"\\n|\\r|\\t")  # Fake line feed etc. using actual backslash and "n"
    multisemipattern = re.compile(r"( *; *)+")
    multispacepattern = re.compile(r"\s+")  # These include fake linefeeds etc with actual backslash-n

    s = " " + s.lower() + " "
    s = breakingseparators.sub(" ; ", s)  # Wanted separators are replaced with a semicolon padded with spaces
    s = markupremovalpattern.sub(" ; ", s)  # Fake linefeeds etc are replaced with a semicolon padded with spaces
    s = unwantedseparators.sub(" ", s)  # Unwanted separators (but not semicolon) are to be replaced with space
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    s = multisemipattern.sub(" ; ", s)  # Replace all semicolon-space sequences with single padded semi
    s = multispacepattern.sub(" ", s)  # Replace all multi-space sequences to single space
    return s

#4-Read our trained data from tokenizer.pickle, classnames.json and model.h5
knownlabels = []  # Index into this to get string label for any numeric tag
labels_index = {}  # dict from string label to numeric tag
labelset = set()  # Used to ensure unique labels while reading file

with open(path + 'tokenizer.pickle',
          'rb') as handle:  # Must use a tokenizer with the same dictionary we used for learning
    tokenizer = pickle.load(handle)  # So we pickled what we learned with and then unpickle it here
with open(path + 'classnames.json', 'r') as classnames_file:
    knownlabels = json.load(classnames_file)  # Load translations from our class indices to practice area names
print(knownlabels)
model = keras.models.load_model(path + "model.h5")  # Load Keras model we learned using KerasPractice notebook
print("Loaded model")

for label in knownlabels:
    tag = len(labels_index)
    labels_index[label] = tag  # Construct dict from label (string) to tag (numerical)


#5-practicearea; predict sector names
def practicearea(text):  # Returns practice are given any text.
    global tokenizer, classnames, model
    sequences = keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=1000)
    prediction = model.predict(sequences)  # Run the actual classification
    return (list(knownlabels.keys())[list(knownlabels.values()).index(np.argmax(prediction))])

#3-Read and prepare job description from Sample file for prediction
def make_label_tensor(doclabels, allknownlabels):
    tensor = np.zeros(len(allknownlabels)).astype(np.float32)
    for i in range(len(allknownlabels)):
        if allknownlabels[i] in doclabels:
            tensor[i] = 1.0
    return tensor

texts = open(path + 'sample', 'r') #sampe is just a text file. You can copy your job description in it.
#texts = reallyclean(texts)
for text in texts:
    print(text)
    print("sad")
    #print(practicearea(text))
texts.close()
