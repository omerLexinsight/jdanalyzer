# this code writes traning logs into csv file
# structure is very flexible, you just need to add dictionary parameters in listofparameterdic

import pandas as pd

resultsdir = '/Users/omerorhan/jdanalyzer/gloves/'
sep = '{#~#}'
filename = 'log_file.csv'

'''
    -savelog4csv accepts two variable
        -resultsdir:path of log file
        -listofparameterdic: python dictionary list for parameters.
'''


def savelog4csv(resultsdir, listofparameterdic):
    df = pd.DataFrame()
    # columnlist list is our variables for picking up values in KerasPracticeJD
    columnlist = ["jobid", "processdate", "word_vectors_count", "text_size", "data_shape", "model_loss",
                  "model_optimizer", "early_stop_patience", "epochs", "batch_size"
                  ,"loss",
                  "val_loss", "categorical_accuracy", "val_categorical_accuracy","f1_score","val_f1_score","precision","val_precision","recall","val_recall", "plot_path", "extra_info",]
    try:
        df = pd.read_csv(resultsdir + filename)  # read csv log file
    except FileNotFoundError:
        df.to_csv(resultsdir + filename)  # in case file is not found, creates a new log file.
    for parameterdic in listofparameterdic:  # iterates each variable
        textlog = ''
        for column in columnlist:
            if parameterdic.get(column):
                textlog = textlog + str(parameterdic.get(column))
            else:
                textlog = textlog + ''
            if column != columnlist[-1]:
                textlog = textlog + sep
        newlist = pd.DataFrame([textlog])[0].str.split(sep, expand=True)
        newlist.columns = columnlist
        df = pd.concat([df, newlist])  # concat all of rows
    df.to_csv(resultsdir + filename, index=False, columns=columnlist)  # save into disk as CSV
