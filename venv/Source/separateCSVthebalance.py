"""
Reading excel file and create a training CSV file.
1-You should use ExcelOperations.py. It includes many useful excel operations.
2-Download excel file to your local machine and change the path.
3-Run readjobfunctionsbythebalancecareers function which reads excel file and creates a dictionary for python file.
combined_csv.csv is your main job descriptions file. Please keep in your mind, they all use the same path.
4-Run separatedSectorJDthebalancecareers for creating a training CSV file.
5-getcountsthebalancecareers gets counts for each job descriptions.

"""
import csv
import sys
import pandas as pd
from ExcelOperations import *

csv.field_size_limit(
    sys.maxsize)  # this size limit is very important, because this program is designed for more than 10 million job description.
#path = "/mnt/usb-ssd/csvfiles/"
path = "/Users/omerorhan/jdanalyzer/gloves/"
excelfile = path + "salesdata/sales.xlsx"


#excelfile = "sales.xlsx"


def isNaN(num):
    return num != num


# reads job fuctions from excel file based on google excel. Before run the function download excel and change the path.
def readjobfunctionsbythebalancecareers():
    diclist = {}
    mustvalues = ""
    datafunctionsprev = ""
    # reads sub functions until find empty rows. If it finds empty rows then looking for job titles.
    for x in range(1, 100):
        datafunctions = readExcelRange(excelfile, "Sales", x, x, 1, 1)
        if str(datafunctions) == "[]" or str(datafunctions[0][0]) == "END":
            break
        if str(datafunctions[0][0]) == "nan":
            # it is for job titles.
            datamust = readExcelRange(excelfile, "Sales", x, x, 2, 2)
            if not str(datamust) == '[]' and not isNaN(datamust[0][0]):
                if len(datamust[0][0]) > 0 and datamust[0][0][-1] == " ":
                    datamust[0][0] = datamust[0][0][0:-1]
                if len(mustvalues) > 0 and mustvalues[0] == " ":
                    datamust[0][0] = datamust[0][0][1]
                mustvalues = mustvalues + (datamust[0][0]) + "|"
        else:
            datafunctionsprev = datafunctions
        # understands for new subfunction
        if readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1) != "nan" and str(
                readExcelRange(excelfile, "Sales", x + 1, x + 1, 1, 1)[0][0]) != "nan":
            if len(mustvalues) > 0 and mustvalues[-1] == "|":
                mustvalues = mustvalues[0:-1]
            diclist.update({str(datafunctionsprev[0][0]): mustvalues})
            mustvalues = ""
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    diclist = {str(k).lower(): str(v).lower() for k, v in diclist.items()}
    return diclist


# creates training file based on listoffunctions.
def separatedSectorJDthebalancecareers(listOfSectors, dataset):
    listofdataset = []
    for key, value in listOfSectors.items():
        salesdt = dataset[dataset['Title'].str.lower().isin(value.split("|"))]
        salesdt['Label'] = key
        salesdt = salesdt.sample(n=25)
        listofdataset.append(salesdt)
    aflac = dataset[dataset['Title'].str.lower().str.contains("aflac insurance sales agent")]
    aflac = aflac['Label'] = "sales representative"
    listofdataset.append(aflac)
    resultdt = pd.concat(listofdataset, ignore_index=True)
    resultdt.to_csv(path + "separated/trainingsales2500jD.csv", index=False)


def getcountsthebalancecareers(listOfSectors, dataset):
    df = pd.DataFrame(columns=list(['Subfunction', 'Title', 'Count']))
    for key, value in listOfSectors.items():
        df = df.append({'Subfunction': str(key), 'Title': "", 'Count': ""}, ignore_index=True)
        totalsubfunction = 0
        for title in value.split("|"):
            salesdt = dataset[dataset['Title'].str.lower() == title]
            totalsubfunction = totalsubfunction + salesdt.shape[0]
            df = df.append({'Subfunction': "", 'Title': str(title), 'Count': str(salesdt.shape[0])}, ignore_index=True)
    print(df)
    df.to_csv(path + "countsofsubfunctions.csv", index=False)


listoftitles = readjobfunctionsbythebalancecareers()

print(listoftitles)
# dataset = pd.read_csv(path + "combined_csv.csv", engine='python',
#                      index_col=False)
# separatedSectorJDthebalancecareers(listoftitles, dataset)

# getcountsthebalancecareers(listoftitles, dataset)
# path = "/Users/omerorhan/jdanalyzer/gloves/"
# dataset = pd.read_csv(path + "trainingjD_ser.csv", engine='python',
#                     index_col=False)

# dataset = dataset[['Label', "Description"]]

# print(dataset["Label"].unique())
# dataset.to_csv(path+ "trainingjD_1.csv", index=False)


def createtrainingfile():
    def separatedSectorJDgenerator(listOfSectors, dataset):
        listofdataset = []
        for key, value in listOfSectors.items():
            salesdt = dataset[dataset['Title'].str.lower().isin(value.split("|"))]
            salesdt['Label'] = key
            listofdataset.append(salesdt)
        aflac = dataset[dataset['Title'].str.lower().str.contains("aflac insurance sales agent", na=False)]
        aflac['Label'] = "sales representative"
        listofdataset.append(aflac)
        salessupport = dataset[dataset['Title'].str.lower().str.contains("sales support", na=False)]
        salessupport['Label'] = "administrative positions related to sales"
        listofdataset.append(salessupport)
        resultdt = pd.concat(listofdataset, ignore_index=True)
        return resultdt

    def chunck_generator(filename, header=False, chunk_size=10 ** 5):
        for chunk in pd.read_csv(filename, delimiter=',', chunksize=chunk_size, iterator=True, parse_dates=[1],
                                 engine='python',
                                 index_col=False):
            yield (chunk)

    def generator(filename, header=False, chunk_size=10 ** 5):
        chunk = chunck_generator(filename, header=False, chunk_size=10 ** 6)
        for row in chunk:
            yield row

    filename = path + "salesdata/combined_csv.csv"
    generator = generator(filename=filename)
    count = 1
    listresults = []
    while True:
        try:
            listresults.append(separatedSectorJDgenerator(listoftitles, next(generator)))
            count = count + 1
            print(count)
        except StopIteration:
            break
    pd.concat(listresults).to_csv(path + "traininggen.csv", index=False)


#createtrainingfile()

#pd.read_csv(path + "traininggen.csv", engine='python', index_col=False)


def getcountofuniqueitem():
    dataset = pd.read_csv(path + "/Users/omerorhan/jdanalyzer/gloves/salesdata/jdbymodel.csv", engine='python', index_col=False)

getcountofuniqueitem()