Create Deep Learning/Keras Model for Practice Area Classification from Job Descriptions
List of components;
-KerasPracticeJD.ipynb: Main modeling file.
    In KerasPracticeJD:
    1-Import Libraries
    2-Environment setting(Server:S, Local:L)
    3-Word embedding settings
    4-Index word vectors
    5-Reading and processing cleaned job descriptions from CSV file
    6-Save classnames.json
    7-Vectorize the text samples into a 2D integer tensor
    8-Split the data into a training set and a validation set
    9-Prepare embedding matrix
    10-Train a 1D convent with global max-pooling
    11-Show plots
    12-Save training result into CSV file
    13-Save model to disk

-ClassifyPractice.py:Prediction file including reading a training file with pickle and making predictions
    In ClassifyPractice.py;
    1-Import Libraries
    2-Environment setting(Server:S, Local:L)
    3-Read and prepare job description from Sample file for prediction
    4-Read our trained data from tokenizer.pickle, classnames.json, and model.h5
    5-practicearea; predict sector names

-CSVConverter.py: Reading different types of job descriptions such as XML and JSON then saving in CSV format.
    list of companies and default format;
         -career_builder_jobs.csv -XML
         -usajobs-job_deduped.csv -XML
         -dice_com-job_us.csv.csv - XML
         -indeed_com-jobs_us.csv- XML
         -monster_com-job.csv - XML
         -jobs_monster-job_deduped_n_merged_20180529_101720743264062 - XML
         -Indeed -JSON
    in CSVConverter.py ;
    1-Convert XML files into CSV (function:mergeCompaniesCSV)
    2-Convert JSON files into CSV(function:JSONtoCSV)
    3-Merge all csv files into one main csv file. (function:mergeCsvFiles)

-log4csv.py: Write training/modelling logs while KerasPracticeJD is running. (in KerasPracticeJD look section-12)
    -savelog4csv can be used for saving log files into CSV.
    -savelog4csv accepts two variables
        -resultsdir:path of log file
        -listofparameterdic: python dictionary for parameters.

-request_Flask.py: Flask request file. This file is for a request flask prediction
    It accepts either file for job description link to send to a server.
    1-Read job descriptions from a file or a link
    2-Send text to Server_Flask
    3-Print a result (response is in JSON format)

-server_FLask.py: Flask server file. It reads a training file from pickle and makes a prediction regarding the request.
    In server_FLask.py file;
    1-Get a request
    2-Send text for a prediction to predictionarea
    3-Read our model data from tokenizer.pickle, classnames.json, and model.h5
    4-Clean and prepare a job description
    5-Make prediction

-separateCSVbySectors.py:This file writes separated CSV files by sector from the main CSV file
    First, it reads job descriptions title and searches keywords for separation
    1-Read main CSV file
    2-Search each sector value in Title
    3-Append each row in a list
    4-Write CSV file in a disk

-prepareTrainingJD.py: it prepares training CSV file from the main CSV file. It includes cleaning as well.
    1-Read main CSV file
    2-Get each row separated by sector. (function:combineSectorJD)
    3-Clean text(function:reallyclean)
    4-Merge into training.csv file
-xml2csv.py: It converts XML files to CSV. It is used by CVSConverter.py
    It can be installed with pip install xml2csv
    when you get 'AttributeError: 'IterParseIterator' object has no attribute 'next'' error
    Just change line 57 in xml2csv.py from this: event, root = self.context.next() to this: event, root = self.context.__next__()
